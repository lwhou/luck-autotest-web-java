package com.quanql.test.core.element;

import org.openqa.selenium.By;

/**
 * 按钮
 *
 *
 */
public class Button extends BaseElement {

  public Button(By by) {
    super(by);
  }

  public Button(String strElement) {
    super(strElement);
  }
}
