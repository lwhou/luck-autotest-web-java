package com.quanql.test.webui.testcase;

import com.quanql.test.webui.base.WebBaseTest;
import com.quanql.test.webui.page.LoginPage;
import org.testng.annotations.Test;

public class DemoHeFei extends WebBaseTest {

    @Test
    public void testSearchNetease() {
        LoginPage.openHeFieEdit();
        LoginPage.WriteUesrName("administrator");
        LoginPage.WritePassWord("123456");
        LoginPage.clickLoginBtn();
        LoginPage.clickMenuFaNode();
        LoginPage.clickChildNode();
        LoginPage.clickStockManagement();
        LoginPage.clickAddBtn();
        LoginPage.addMaterialDetailed();
        LoginPage.searchMaterial("高分子钛");
        LoginPage.wait(1000);
        LoginPage.selectMaterial();
        LoginPage.wait(1000);
        LoginPage.operateMaterial();
        LoginPage.editMaterialDetails(Integer.toString(100),Integer.toString(5),Integer.toString(1000),"2022-07-29");
        LoginPage.saveInfo();
        LoginPage.wait(1500);
        LoginPage.exit();
    }
}
