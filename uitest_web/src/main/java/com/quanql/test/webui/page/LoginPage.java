package com.quanql.test.webui.page;

import com.quanql.test.core.utils.LogUtil;
import com.quanql.test.webui.base.WebBasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class LoginPage extends WebBasePage {

    private static final String BTN_LOGIN_XPATH ="//*[@id=\"form\"]/div[3]/div/div/span/button";
    private static final String BTN_MENU_XPATH ="//*[@id=\"root\"]/section/section/aside[1]/div[1]/ul/li[8]/div/span/span";
    private static final String BTN_MENU_CHILD_XPATH ="//*[@id=\"429$Menu\"]/li[3]/div/span/span";
    private static final String BTN_Stock_XPATH ="//*[@id=\"671$Menu\"]/li[2]/span";
    private static final String BTN_ADD_XPATH ="//*[@id=\"root\"]/section/section/section/main/div/div/div/div[1]/div/div[2]/div[1]/div[1]/div/div/span[1]/button";
    private static final String BTN_MATERIAL_XPATH ="/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[1]/div/div/button";
    private static final String searchMaterial_XPATH ="/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[1]/div/span/input";
    private static final String selectMaterial_XPATH ="/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div/div/div/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div/div/span";
    private static final String BTN_confirm_XPATH ="/html/body/div[4]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div[3]/div/div[2]/button[2]";
    private static final String BTN_operate_XPATH ="/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div/div[1]/div/div/div/div/div[2]/div[1]/div[3]/div[3]/div/div/div/div/span/a";
    private static final String BTN_edit_XPATH ="/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div/div[2]/a";
    private static final String data_XPATH ="//*[@id=\"dproduct\"]";
    private static final String BTN_save_XPATH ="/html/body/div[4]/div/div[2]/div/div[2]/div[3]/button";
    private static final String BTN_time_XPATH ="/html/body/div[5]/div/div/div/div/div[1]/div/input";
    private static final String BTN_saveInfo_XPATH ="/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div[1]/div/div/div/div/span[2]/button";
    private static final String BTN_exit_XPATH ="/html/body/div[3]/div/div[2]/div/div[2]/button";

    public static void openHeFieEdit() {
        LogUtil.info("通过url架修首页");
        baseOpt.open("http://10.10.10.120:32018/login");
    }
    public static void WriteUesrName(String username) {
        LogUtil.info("写账号");
        baseOpt.input(By.id("username"),username);
    }
    public static void WritePassWord(String password) {
        LogUtil.info("密码");
        baseOpt.input(By.id("password"),password);
    }

    public static void clickLoginBtn() {
        LogUtil.info("点击登录按钮");
        baseOpt.click(By.xpath(BTN_LOGIN_XPATH));
    }
    public static void clickMenuFaNode() {
        LogUtil.info("展开菜单物资管理");
        baseOpt.click(By.xpath(BTN_MENU_XPATH));
    }
    public static void clickChildNode() {
        LogUtil.info("展开子菜单入库管理");
        baseOpt.click(By.xpath(BTN_MENU_CHILD_XPATH));
    }
    public static void clickStockManagement() {
        LogUtil.info("进入快速入库");
        baseOpt.click(By.xpath(BTN_Stock_XPATH));
    }
    public static void clickAddBtn() {
        LogUtil.info("新增");
        baseOpt.click(By.xpath(BTN_ADD_XPATH));
    }
    public static void addMaterialDetailed() {
        LogUtil.info("新增物料明细");
        baseOpt.click(By.xpath(BTN_MATERIAL_XPATH));
    }
    public static void searchMaterial(String product) {
        LogUtil.info("搜索物料");
        baseOpt.input(By.xpath(searchMaterial_XPATH),product);
    }
    public static void selectMaterial() {
        LogUtil.info("选择物料");
        baseOpt.click(By.xpath(selectMaterial_XPATH));
//        WebElement element = baseOpt.findElement(By.xpath(selectMaterial2_XPATH));
//        baseOpt.getDriver().executeScript("arguments[0].click();", element);
        baseOpt.click(By.xpath(BTN_confirm_XPATH));
    }

    public static void operateMaterial(){
        LogUtil.info("移动修改");
        WebElement element = baseOpt.findElement(By.xpath(BTN_operate_XPATH));
//        baseOpt.click(By.xpath(BTN_operate_XPATH));
        Actions action = new Actions(baseOpt.getDriver());
        action.moveToElement(element).perform();
        baseOpt.click(By.xpath(BTN_edit_XPATH));
    }

    public static void editMaterialDetails(String ivaliday,String iqtyUnloadIn,String ipriceSum,String data){
        LogUtil.info("移动修改详细数据");
        baseOpt.input(By.id("ivaliday"),ivaliday);
        baseOpt.input(By.id("iqtyUnloadIn"),iqtyUnloadIn);
        baseOpt.input(By.id("ipriceSum"),ipriceSum);

//        baseOpt.input(By.xpath(data_XPATH),data);
//        WebElement element = baseOpt.findElement(By.xpath(data_XPATH));
//        baseOpt.input(By.xpath(data_XPATH),element,data);

        baseOpt.click(By.xpath(data_XPATH));
        baseOpt.input(By.xpath(BTN_time_XPATH),data);
        baseOpt.click(By.xpath(BTN_save_XPATH));

    }

    public static void saveInfo(){
        LogUtil.info("保存");
        baseOpt.click(By.xpath(BTN_saveInfo_XPATH));
    }

    public static void exit(){
        LogUtil.info("退出");
        baseOpt.click(By.xpath(BTN_exit_XPATH));
    }


}
