package com.quanql.test.webui.base;

import com.quanql.test.core.base.BaseTest;

/**
 * testcase的基类<br>
 * testng生命周期公共部分
 *
 */
public class WebBaseTest extends BaseTest {}
